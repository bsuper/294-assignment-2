import numpy as np

from cs294_129.layers import *
from cs294_129.fast_layers import *
from cs294_129.layer_utils import *


class ThreeLayerConvNet(object):
  """
  A three-layer convolutional network with the following architecture:

  conv - relu - 2x2 max pool - affine - relu - affine - softmax

  The network operates on minibatches of data that have shape (N, C, H, W)
  consisting of N images, each with height H and width W and with C input
  channels.
  """

  def __init__(self,
               input_dim=(3, 32, 32),
               num_filters=32,
               filter_size=7,
               hidden_dim=100,
               num_classes=10,
               weight_scale=1e-3,
               reg=0.0,
               use_batchnorm=False,
               dtype=np.float32):
    """
    Initialize a new network.

    Inputs:
    - input_dim: Tuple (C, H, W) giving size of input data
    - num_filters: Number of filters to use in the convolutional layer
    - filter_size: Size of filters to use in the convolutional layer
    - hidden_dim: Number of units to use in the fully-connected hidden layer
    - num_classes: Number of scores to produce from the final affine layer.
    - weight_scale: Scalar giving standard deviation for random initialization
      of weights.
    - reg: Scalar giving L2 regularization strength
    - dtype: numpy datatype to use for computation.
    """
    self.params = {}
    self.reg = reg
    self.dtype = dtype
    self.use_batchnorm = use_batchnorm

    ############################################################################
    # TODO: Initialize weights and biases for the three-layer convolutional    #
    # network. Weights should be initialized from a Gaussian with standard     #
    # deviation equal to weight_scale; biases should be initialized to zero.   #
    # All weights and biases should be stored in the dictionary self.params.   #
    # Store weights and biases for the convolutional layer using the keys 'W1' #
    # and 'b1'; use keys 'W2' and 'b2' for the weights and biases of the       #
    # hidden affine layer, and keys 'W3' and 'b3' for the weights and biases   #
    # of the output affine layer.                                              #
    ############################################################################

    # pass conv_param to the forward pass for the convolutional layer
    self.conv_param = {'stride': 1, 'pad': (filter_size - 1) / 2}

    # pass pool_param to the forward pass for the max-pooling layer
    self.pool_param = {'pool_height': 2, 'pool_width': 2, 'stride': 2}

    l1_out_h, l1_out_w, l1_out_d = compute_conv_dim(
        input_dim[1],
        input_dim[2],
        input_dim[0],
        num_filters,
        filter_size,
        filter_size,
        stride=self.conv_param['stride'],
        pad=self.conv_param['pad'],
        max_pool_h=self.pool_param['pool_height'],
        max_pool_w=self.pool_param['pool_width'],
        max_pool_stride=self.pool_param['stride'])

    self.params['W1'] = np.random.randn(num_filters, input_dim[0], filter_size,
                                        filter_size) * weight_scale
    self.params['W2'] = np.random.randn(l1_out_h * l1_out_w * l1_out_d,
                                        hidden_dim) * weight_scale
    self.params['W3'] = np.random.randn(hidden_dim, num_classes) * weight_scale

    self.params['b1'] = np.zeros(num_filters)
    self.params['b2'] = np.zeros(hidden_dim)
    self.params['b3'] = np.zeros(num_classes)

    if use_batchnorm:
      self.params['gamma1'] = np.ones(num_filters)
      self.params['beta1'] = np.zeros(num_filters)
      self.params['gamma2'] = np.ones(hidden_dim)
      self.params['beta2'] = np.zeros(hidden_dim)
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # With batch normalization we need to keep track of running means and
    # variances, so we need to pass a special bn_param object to each batch
    # normalization layer. You should pass self.bn_params[0] to the forward pass
    # of the first batch normalization layer, self.bn_params[1] to the forward
    # pass of the second batch normalization layer, etc.
    self.bn_params = []
    if self.use_batchnorm:
      self.bn_params = [{'mode': 'train'} for i in xrange(2)]

    for k, v in self.params.iteritems():
      self.params[k] = v.astype(dtype)

  def loss(self, X, y=None):
    """
    Evaluate loss and gradient for the three-layer convolutional network.

    Input / output: Same API as TwoLayerNet in fc_net.py.
    """
    X = X.astype(self.dtype)
    mode = 'test' if y is None else 'train'

    W1, b1 = self.params['W1'], self.params['b1']
    W2, b2 = self.params['W2'], self.params['b2']
    W3, b3 = self.params['W3'], self.params['b3']
    if self.use_batchnorm:
      gamma1, beta1 = self.params['gamma1'], self.params['beta1']
      gamma2, beta2 = self.params['gamma2'], self.params['beta2']
      for bn_param in self.bn_params:
        bn_param[mode] = mode

    scores = None
    ############################################################################
    # TODO: Implement the forward pass for the three-layer convolutional net,  #
    # computing the class scores for X and storing them in the scores          #
    # variable.                                                                #
    ############################################################################
    if self.use_batchnorm:
      out1, cache1 = conv_sbn_relu_pool_forward(
          X, W1, b1, gamma1, beta1, self.conv_param, self.pool_param,
          self.bn_params[0])
      out2, cache2 = affine_bn_relu_forward(out1, W2, b2, gamma2, beta2,
                                            self.bn_params[1])
    else:
      out1, cache1 = conv_relu_pool_forward(X, W1, b1, self.conv_param,
                                            self.pool_param)
      out2, cache2 = affine_relu_forward(out1, W2, b2)
    out3, cache3 = affine_forward(out2, W3, b3)
    scores = out3
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    if y is None:
      return scores

    loss, grads = 0, {}
    ############################################################################
    # TODO: Implement the backward pass for the three-layer convolutional net, #
    # storing the loss and gradients in the loss and grads variables. Compute  #
    # data loss using softmax, and make sure that grads[k] holds the gradients #
    # for self.params[k]. Don't forget to add L2 regularization!               #
    ############################################################################
    loss, dscores = softmax_loss(scores, y)
    loss += 0.5 * self.reg * (
        np.sum(W1 * W1) + np.sum(W2 * W2) + np.sum(W3 * W3))

    dout3, dW3, db3 = affine_backward(dscores, cache3)
    if self.use_batchnorm:
      dout2, dW2, db2, dgamma2, dbeta2 = affine_bn_relu_backward(dout3, cache2)
      dX, dW1, db1, dgamma1, dbeta1 = conv_sbn_relu_pool_backward(dout2, cache1)
      grads['gamma2'], grads['gamma1'] = dgamma2, dgamma1
      grads['beta2'], grads['beta1'] = dbeta2, dbeta1
    else:
      dout2, dW2, db2 = affine_relu_backward(dout3, cache2)
      dX, dW1, db1 = conv_relu_pool_backward(dout2, cache1)

    grads['W3'] = dW3 + self.reg * W3
    grads['W2'] = dW2 + self.reg * W2
    grads['W1'] = dW1 + self.reg * W1
    grads['b3'], grads['b2'], grads['b1'] = db3, db2, db1
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads


class MySuperConvNet(object):
  """
  A three-layer convolutional network with the following architecture:

  [conv - sbn - relu - 2x2 max pool]x2 + [affine - bn - relu]x2
    + affine - softmax

  Achieves 74% validation accuracy after 5 epochs.

  The network operates on minibatches of data that have shape (N, C, H, W)
  consisting of N images, each with height H and width W and with C input
  channels.
  """

  def __init__(self,
               input_dim=(3, 32, 32),
               conv1_num_filters=64,
               conv1_filter_size=5,
               conv2_num_filters=64,
               conv2_filter_size=5,
               hidden_dim=200,
               num_classes=10,
               weight_scale=1e-3,
               reg=0.0,
               conv1_param={'stride': 1,
                            'pad': 0},
               conv2_param={'stride': 1,
                            'pad': 0},
               pool_param={'pool_height': 2,
                           'pool_width': 2,
                           'stride': 2},
               dtype=np.float32):
    """
    Initialize a new network.

    Inputs:
    - input_dim: Tuple (C, H, W) giving size of input data
    - num_filters: Number of filters to use in the convolutional layer
    - conv1_filter_size: Size of filters to use in the convolutional layer
    - hidden_dim: Number of units to use in the fully-connected hidden layer
    - num_classes: Number of scores to produce from the final affine layer.
    - weight_scale: Scalar giving standard deviation for random initialization
      of weights.
    - reg: Scalar giving L2 regularization strength
    - dtype: numpy datatype to use for computation.
    """
    self.params = {}
    self.reg = reg
    self.dtype = dtype

    ############################################################################
    # TODO: Initialize weights and biases for the three-layer convolutional    #
    # network. Weights should be initialized from a Gaussian with standard     #
    # deviation equal to weight_scale; biases should be initialized to zero.   #
    # All weights and biases should be stored in the dictionary self.params.   #
    # Store weights and biases for the convolutional layer using the keys 'W1' #
    # and 'b1'; use keys 'W2' and 'b2' for the weights and biases of the       #
    # hidden affine layer, and keys 'W3' and 'b3' for the weights and biases   #
    # of the output affine layer.                                              #
    ############################################################################

    # pass conv_param to the forward pass for the convolutional layer
    self.conv1_param, self.conv2_param = conv1_param, conv2_param

    # pass pool_param to the forward pass for the max-pooling layer
    self.pool_param = pool_param

    # [L1] INPUT: N x 3 x 32 x 32 | OUTPUT: N x 64 x 14 x 14
    # WEIGHTS: 64 x 3 x 5 x 5
    self.params['W1'] = np.random.randn(conv1_num_filters, input_dim[0],
                                        conv1_filter_size,
                                        conv1_filter_size) * weight_scale
    l1_out_h, l1_out_w, l1_out_d = compute_conv_dim(
        input_dim[1],
        input_dim[2],
        input_dim[0],
        conv1_num_filters,
        conv1_filter_size,
        conv1_filter_size,
        stride=self.conv1_param['stride'],
        pad=self.conv1_param['pad'],
        max_pool_h=self.pool_param['pool_height'],
        max_pool_w=self.pool_param['pool_width'],
        max_pool_stride=self.pool_param['stride'])
    # [L2] INPUT: N x 64 x 14 x 14 | OUTPUT: N x 64 x 5 x 5
    # WEIGHTS: 64 x 64 x 5 X 5
    self.params['W2'] = np.random.randn(conv2_num_filters, conv1_num_filters,
                                        conv2_filter_size,
                                        conv2_filter_size) * weight_scale
    l2_out_h, l2_out_w, l2_out_d = compute_conv_dim(
        l1_out_h,
        l1_out_w,
        l1_out_d,
        conv2_num_filters,
        conv2_filter_size,
        conv2_filter_size,
        stride=self.conv2_param['stride'],
        pad=self.conv2_param['pad'],
        max_pool_h=self.pool_param['pool_height'],
        max_pool_w=self.pool_param['pool_width'],
        max_pool_stride=self.pool_param['stride'])
    # [L3] INPUT: N x (64x5x5) | OUTPUT: N x 200 | WEIGHTS: (64x5x5) x 200
    self.params['W3'] = np.random.randn(l2_out_h * l2_out_w * l2_out_d,
                                        hidden_dim) * weight_scale
    # [L4] INPUT: N x 200 | OUTPUT: N x 200 | WEIGHTS: 200 x 200
    self.params['W4'] = np.random.randn(hidden_dim, hidden_dim) * weight_scale
    # [L5] INPUT: N x 200 | OUTPUT: N x 10 | WEIGHTS: 200 x 10
    self.params['W5'] = np.random.randn(hidden_dim, num_classes) * weight_scale

    self.params['b1'] = np.zeros(conv1_num_filters)
    self.params['b2'] = np.zeros(conv2_num_filters)
    self.params['b3'] = np.zeros(hidden_dim)
    self.params['b4'] = np.zeros(hidden_dim)
    self.params['b5'] = np.zeros(num_classes)

    self.params['gamma1'] = np.ones(conv1_num_filters)
    self.params['beta1'] = np.zeros(conv1_num_filters)
    self.params['gamma2'] = np.ones(conv2_num_filters)
    self.params['beta2'] = np.zeros(conv2_num_filters)
    self.params['gamma3'] = np.ones(hidden_dim)
    self.params['beta3'] = np.zeros(hidden_dim)
    self.params['gamma4'] = np.ones(hidden_dim)
    self.params['beta4'] = np.zeros(hidden_dim)
    self.bn_params = []
    # Batchnorm for 2 convolutions and 2 affine layers is 4 batchnorms total.
    self.bn_params = [{'mode': 'train'} for i in xrange(4)]
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    for k, v in self.params.iteritems():
      self.params[k] = v.astype(dtype)

  def loss(self, X, y=None):
    """
    Evaluate loss and gradient for the three-layer convolutional network.

    Input / output: Same API as TwoLayerNet in fc_net.py.
    """
    W1, b1 = self.params['W1'], self.params['b1']
    W2, b2 = self.params['W2'], self.params['b2']
    W3, b3 = self.params['W3'], self.params['b3']
    W4, b4 = self.params['W4'], self.params['b4']
    W5, b5 = self.params['W5'], self.params['b5']
    gamma1, beta1 = self.params['gamma1'], self.params['beta1']
    gamma2, beta2 = self.params['gamma2'], self.params['beta2']
    gamma3, beta3 = self.params['gamma3'], self.params['beta3']
    gamma4, beta4 = self.params['gamma4'], self.params['beta4']
    scores = None

    mode = 'test' if y is None else 'train'
    for bn_param in self.bn_params:
      bn_param[mode] = mode
    ############################################################################
    # TODO: Implement the forward pass for the three-layer convolutional net,  #
    # computing the class scores for X and storing them in the scores          #
    # variable.                                                                #
    ############################################################################
    out1, cache1 = conv_sbn_relu_pool_forward(X, W1, b1, gamma1, beta1,
                                              self.conv1_param, self.pool_param,
                                              self.bn_params[0])
    out2, cache2 = conv_sbn_relu_pool_forward(out1, W2, b2, gamma2, beta2,
                                              self.conv2_param, self.pool_param,
                                              self.bn_params[1])
    out3, cache3 = affine_bn_relu_forward(out2, W3, b3, gamma3, beta3,
                                          self.bn_params[2])
    out4, cache4 = affine_bn_relu_forward(out3, W4, b4, gamma4, beta4,
                                          self.bn_params[3])
    out5, cache5 = affine_forward(out4, W5, b5)
    scores = out5
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    if y is None:
      return scores

    loss, grads = 0, {}
    ############################################################################
    # TODO: Implement the backward pass for the three-layer convolutional net, #
    # storing the loss and gradients in the loss and grads variables. Compute  #
    # data loss using softmax, and make sure that grads[k] holds the gradients #
    # for self.params[k]. Don't forget to add L2 regularization!               #
    ############################################################################
    loss, dscores = softmax_loss(scores, y)
    loss += 0.5 * self.reg * (np.sum(W1 * W1) + np.sum(W2 * W2) + np.sum(
        W3 * W3) + np.sum(W4 * W4) + np.sum(W5 * W5))

    dout5, dW5, db5 = affine_backward(dscores, cache5)
    dout4, dW4, db4, dgamma4, dbeta4 = affine_bn_relu_backward(dout5, cache4)
    dout3, dW3, db3, dgamma3, dbeta3 = affine_bn_relu_backward(dout4, cache3)
    dout2, dW2, db2, dgamma2, dbeta2 = conv_sbn_relu_pool_backward(dout3,
                                                                   cache2)
    dX, dW1, db1, dgamma1, dbeta1 = conv_sbn_relu_pool_backward(dout2, cache1)
    grads['gamma4'], grads['gamma3'] = dgamma4, dgamma3
    grads['gamma2'], grads['gamma1'] = dgamma2, dgamma1
    grads['beta4'], grads['beta3'] = dbeta4, dbeta3
    grads['beta2'], grads['beta1'] = dbeta2, dbeta1

    grads['W5'] = dW5 + self.reg * W5
    grads['W4'] = dW4 + self.reg * W4
    grads['W3'] = dW3 + self.reg * W3
    grads['W2'] = dW2 + self.reg * W2
    grads['W1'] = dW1 + self.reg * W1
    grads['b5'] = db5
    grads['b4'], grads['b3'], grads['b2'], grads['b1'] = db4, db3, db2, db1
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads
