from cs294_129.layers import *
from cs294_129.fast_layers import *


def affine_relu_forward(x, w, b):
  """
  Convenience layer that perorms an affine transform followed by a ReLU

  Inputs:
  - x: Input to the affine layer
  - w, b: Weights for the affine layer

  Returns a tuple of:
  - out: Output from the ReLU
  - cache: Object to give to the backward pass
  """
  a, fc_cache = affine_forward(x, w, b)
  out, relu_cache = relu_forward(a)
  cache = (fc_cache, relu_cache)
  return out, cache


def affine_relu_backward(dout, cache):
  """
  Backward pass for the affine-relu convenience layer
  """
  fc_cache, relu_cache = cache
  da = relu_backward(dout, relu_cache)
  dx, dw, db = affine_backward(da, fc_cache)
  return dx, dw, db


def affine_bn_relu_forward(x, w, b, gamma, beta, bn_param):
  a, fc_cache = affine_forward(x, w, b)
  bn, bn_cache = batchnorm_forward(a, gamma, beta, bn_param)
  out, relu_cache = relu_forward(bn)
  cache = (fc_cache, bn_cache, relu_cache)
  return out, cache


def affine_bn_relu_backward(dout, cache):
  fc_cache, bn_cache, relu_cache = cache
  dbn = relu_backward(dout, relu_cache)
  da, dgamma, dbeta = batchnorm_backward_alt(dbn, bn_cache)
  dx, dw, db = affine_backward(da, fc_cache)
  return dx, dw, db, dgamma, dbeta


def conv_relu_forward(x, w, b, conv_param):
  """
  A convenience layer that performs a convolution followed by a ReLU.

  Inputs:
  - x: Input to the convolutional layer
  - w, b, conv_param: Weights and parameters for the convolutional layer

  Returns a tuple of:
  - out: Output from the ReLU
  - cache: Object to give to the backward pass
  """
  a, conv_cache = conv_forward_fast(x, w, b, conv_param)
  out, relu_cache = relu_forward(a)
  cache = (conv_cache, relu_cache)
  return out, cache


def conv_relu_backward(dout, cache):
  """
  Backward pass for the conv-relu convenience layer.
  """
  conv_cache, relu_cache = cache
  da = relu_backward(dout, relu_cache)
  dx, dw, db = conv_backward_fast(da, conv_cache)
  return dx, dw, db


def conv_relu_pool_forward(x, w, b, conv_param, pool_param):
  """
  Convenience layer that performs a convolution, a ReLU, and a pool.

  Inputs:
  - x: Input to the convolutional layer
  - w, b, conv_param: Weights and parameters for the convolutional layer
  - pool_param: Parameters for the pooling layer

  Returns a tuple of:
  - out: Output from the pooling layer
  - cache: Object to give to the backward pass
  """
  a, conv_cache = conv_forward_fast(x, w, b, conv_param)
  s, relu_cache = relu_forward(a)
  out, pool_cache = max_pool_forward_fast(s, pool_param)
  cache = (conv_cache, relu_cache, pool_cache)
  return out, cache


def conv_relu_pool_backward(dout, cache):
  """
  Backward pass for the conv-relu-pool convenience layer
  """
  conv_cache, relu_cache, pool_cache = cache
  ds = max_pool_backward_fast(dout, pool_cache)
  da = relu_backward(ds, relu_cache)
  dx, dw, db = conv_backward_fast(da, conv_cache)
  return dx, dw, db


def conv_sbn_relu_forward(x, w, b, gamma, beta, conv_param, bn_param):
  """
  A convenience layer that performs a convolution followed by a spatial
  batchnorm and ReLU.

  Inputs:
  - x: Input to the convolutional layer
  - w, b, conv_param: Weights and parameters for the convolutional layer

  Returns a tuple of:
  - out: Output from the ReLU
  - cache: Object to give to the backward pass
  """
  a, conv_cache = conv_forward_fast(x, w, b, conv_param)
  sbn, sbn_cache = spatial_batchnorm_forward(a, gamma, beta, bn_param)
  out, relu_cache = relu_forward(sbn)
  cache = (conv_cache, sbn_cache, relu_cache)
  return out, cache


def conv_sbn_relu_backward(dout, cache):
  """
  Backward pass for the conv-sbn-relu convenience layer.
  """
  conv_cache, sbn_cache, relu_cache = cache
  da = relu_backward(dout, relu_cache)
  dsbn, dgamma, dbeta = spatial_batchnorm_backward(da, sbn_cache)
  dx, dw, db = conv_backward_fast(dsbn, conv_cache)
  return dx, dw, db, dgamma, dbeta


def conv_sbn_relu_pool_forward(x, w, b, gamma, beta, conv_param, pool_param,
                               bn_param):
  """
  Convenience layer that performs a convolution, SBN, a ReLU, and a pool.

  Inputs:
  - x: Input to the convolutional layer
  - w, b, conv_param: Weights and parameters for the convolutional layer
  - pool_param: Parameters for the pooling layer

  Returns a tuple of:
  - out: Output from the pooling layer
  - cache: Object to give to the backward pass
  """
  a, conv_cache = conv_forward_fast(x, w, b, conv_param)
  sbn, sbn_cache = spatial_batchnorm_forward(a, gamma, beta, bn_param)
  s, relu_cache = relu_forward(sbn)
  out, pool_cache = max_pool_forward_fast(s, pool_param)
  cache = (conv_cache, sbn_cache, relu_cache, pool_cache)
  return out, cache


def conv_sbn_relu_pool_backward(dout, cache):
  """
  Backward pass for the conv-sbn-relu-pool convenience layer
  """
  conv_cache, sbn_cache, relu_cache, pool_cache = cache
  ds = max_pool_backward_fast(dout, pool_cache)
  da = relu_backward(ds, relu_cache)
  dsbn, dgamma, dbeta = spatial_batchnorm_backward(da, sbn_cache)
  dx, dw, db = conv_backward_fast(dsbn, conv_cache)
  return dx, dw, db, dgamma, dbeta


def compute_conv_dim(h1,
                     w1,
                     d1,
                     num_filters,
                     f_h,
                     f_w,
                     stride=1,
                     pad=0,
                     max_pool_h=0,
                     max_pool_w=0,
                     max_pool_stride=0):
  # Convolution
  d2 = num_filters
  h2_num = h1 - f_h + 2 * pad
  assert h2_num % stride == 0, "H1 - F_H + 2*P is not divisible by stride"
  h2 = h2_num / stride + 1
  w2_num = w1 - f_w + 2 * pad
  assert w2_num % stride == 0, "W1 - F_W + 2*P is not divisible by stride"
  w2 = w2_num / stride + 1

  # Max Pool
  if max([max_pool_h, max_pool_w, max_pool_stride]) > 0:
    h3_num = h2 - max_pool_h
    assert h3_num % max_pool_stride == 0, "H2 - POOL_H not divisible by stride"
    h3 = h3_num / max_pool_stride + 1
    w3_num = w2 - max_pool_w
    assert w3_num % max_pool_stride == 0, "W2 - POOL_W not divisible by stride"
    w3 = w3_num / max_pool_stride + 1
    d3 = d2
    return h3, w3, d3
  else:
    return h2, w2, d2
